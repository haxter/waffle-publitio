defmodule Waffle.Storage.Publitio do
  @moduledoc ~S"""
  TODO 
  - [ ] เปลี่ยนbase url
  - [ ] signed url
  - [ ] delete
  """
  # Upload only the original file as we will use url-based transformations.
  def put(definition, version = :original, {file, scope}) do
    {:ok, file} =
      Publitio.File.create(file.path, %{
        folder: destination_dir(definition, version, {file, scope}),
        public_id: definition.filename(version, {file, scope}),
        privacy: definition.privacy(),
        option_hls: definition.option_hls(),
        option_ad: definition.option_ad(),
        option_download: definition.option_download(),
        option_transform: definition.option_transform()
      })

    {:ok, file.public_id}
  end

  def put(_, _version, {file, _}) do
    {:ok, file.file_name}
  end

  def url(definition, version, {file, scope}, _options \\ []) do
    opts =
      %{
        folder: destination_dir(definition, version, {file, scope}),
        asset_host: definition.asset_host
      }
      |> Map.merge(definition.url_based_transform(version, {file, scope}))

    Publitio.Url.for(file.file_name, opts)
  end

  defp destination_dir(definition, version, {file, scope}) do
    Path.join(definition.storage_dir_prefix(), definition.storage_dir(version, {file, scope}))
  end
end
