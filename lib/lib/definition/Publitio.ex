defmodule Waffle.Definition.Publitio do
  defmacro __using__(_options) do
    quote do
      @before_compile Waffle.Definition.Publitio
      @privacy :private
      @option_download :enabled
      @option_transform :enabled
      @option_ad :enabled
      @option_hls :disabled
      @asset_host "https://media.publit.io"
    end
  end

  defmacro __before_compile__(_env) do
    quote do
      def url_based_transform(_, _), do: %{}
      def privacy, do: @privacy
      def option_download, do: @option_download
      def option_transform, do: @option_transform
      def option_ad, do: @option_ad
      def option_hls, do: @option_hls
    end
  end
end
